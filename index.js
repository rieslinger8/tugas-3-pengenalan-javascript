// Soal no 1
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";
// gabungkan variabel-variabel tersebut agar menghasilkan output
// saya senang belajar JAVASCRIPT
// Jawaban soal no 1

var pertamaConc = pertama.substring(0, 5);
var keduaConc = pertama.substring(12, 19);
var ketigaConc = kedua.substring(0, 8);
var keempatConc = kedua.substring(8, 18).toUpperCase();
console.log(pertamaConc + keduaConc + ketigaConc + keempatConc);


// Soal no 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";
// ubahlah variabel diatas ke dalam integer dan lakukan operasi matematika semua variabel agar menghasilkan output 24 (integer).
//*catatan :
// 1. gunakan 3 operasi, tidak boleh  lebih atau kurang. contoh : 10 + 2 * 4 + 6
// 2. Boleh menggunakan tanda kurung . contoh : (10 + 2 ) * (4 + 6)
// Jawaban soal no 2

var kataPertamaInt = parseInt(kataPertama);
var kataKeduaInt = parseInt(kataKedua);
var kataKetigaInt = parseInt(kataKetiga);
var kataKeempatInt = parseInt(kataKeempat);
var tambah = parseInt(kataPertamaInt + kataKeduaInt * kataKetigaInt + kataKeempatInt);
console.log(tambah);

// Soal no 3
var kalimat = 'wah javascript itu keren sekali'; 
// selesaikan variabel yang belum diisi dan hasilkan output seperti berikut:
var kataPertama = kalimat.substring(0, 3); 
var kataKedua; // do your own! 
var kataKetiga; // do your own! 
var kataKeempat; // do your own! 
var kataKelima; // do your own! 

// Kata Pertama: wah
// Kata Kedua: javascript
// Kata Ketiga: itu
// Kata Keempat: keren
// Kata Kelima: sekali

// Jawaban no 3

kataKedua = kalimat.substring(3, 14); 
kataKetiga = kalimat.substring(15, 18);
kataKeempat = kalimat.substring(19 ,24);
kataKelima = kalimat.substring(25, 32);

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);

